// import the http and uuid modules
const http = require("http");
const uuid = require("uuid");

// create new server with http method createServer
const newServer = http.createServer((request, response) => {
  let displayContent = `<!DOCTYPE html>
    <html>
      <head>
      </head>
      <body>
          <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
          <p> - Martin Fowler</p>
      </body>
    </html>`;

  //Should return the following JSON string
  let jsonContent = JSON.stringify({
    slideshow: {
      author: "Yours Truly",
      date: "date of publication",
      slides: [
        {
          title: "Wake up to WonderWidgets!",
          type: "all",
        },
        {
          items: [
            "Why <em>WonderWidgets</em> are great",
            "Who <em>buys</em> WonderWidgets",
          ],
          title: "Overview",
          type: "all",
        },
      ],
      title: "Sample Slide Show",
    },
  });
  //printing html content through response method
  if (request.method === "GET" && request.url === "/html") {
    response.setHeader("Content-Type", "text/html");
    response.end(displayContent);
  } else if (request.method === "GET" && request.url === "/json") {
    response.setHeader("Content-Type", "application/json");
    response.end(jsonContent); // =>print the json content
  } else if (request.method === "GET" && request.url === "/") {
    response.setHeader("Content-Type", "text/plain");
    response.end("Home Page"); // if only "/" is used it print home page
  } else if (request.method === "GET" && request.url === "/uuid") {
    const uuidContent = uuid.v4(); // uuid.v4 method is used to console the uuid's
    const jsonResponse = JSON.stringify({ uuid: uuidContent });
    response.end(jsonResponse); //=> console the uuid's
  } else if (request.method === "GET" && request.url.startsWith("/status/")) {
    const urlArray = request.url.split("/"); // => url split by "/" it make a array
    const statusCode = parseInt(urlArray[2]); // last array is store which is number code
    if (!isNaN(statusCode) && statusCode >= 100 && statusCode <= 500) {
      const jsonResponse = JSON.stringify({ status: statusCode });
      response.end(jsonResponse);
    } else {
      response.end("404 Error");
    }
    // }
  } else if (request.method === "GET" && request.url.startsWith("/delay/")) {
    const urlDelayArray = request.url.split("/");
    const delayTime = parseInt(urlDelayArray[2]);
    if (!isNaN(delayTime)) {
      setTimeout(() => {
        response.end("The Message Is SuccessFul..!");
      }, delayTime * 1000);
    } else {
      response.end("Error 404");
    }
  } else {
    response.statusCode = 404;
    response.end("Not Found");
  }
});

//Set the port number for the server to listen on
const PORT = 8000;

//Start the server and listen for incoming requests on the specified port
newServer.listen(PORT, () => console.log(`Server started on port ${PORT}`));
